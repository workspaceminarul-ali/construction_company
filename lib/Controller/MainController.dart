import 'package:construction_company/Widgets/ResultPage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MainController extends GetxController{
  TabController tabController;
  TextEditingController constructionAreaController;
  TextEditingController exteriorSlidingController;
  TextEditingController flooringController;
  TextEditingController foundationController;
  TextEditingController garagesController;
  TextEditingController gradationController;
  TextEditingController hVACController;

  List<String> lsData = [
    "ConstructionArea",
    "Gradation",
    "Foundation",
    "ExteriorSliding",
    "HVAC",
    "Flooring",
    "Garages",
  ];
  List<Widget> buildTabs() {
    List<Tab> tabList = <Tab>[];
    tabList.clear();
    lsData.forEach((element) {
      tabList.add(
        Tab(
          child: Text(
            element,
            style: TextStyle(color: Colors.black),
          ),
        ),
      );
    });
    return tabList;
  }
  @override
  void onInit() {
    super.onInit();
    constructionAreaController = TextEditingController();
    exteriorSlidingController = TextEditingController();
    flooringController = TextEditingController();
    foundationController = TextEditingController();
    garagesController = TextEditingController();
    gradationController = TextEditingController();
    hVACController = TextEditingController();
  }

  onNext(){
    if(tabController.previousIndex == 5){
      Get.to(() => ResultPage());
    }else{
      tabController.animateTo((tabController.index + 1));
    }

  }
  onSkip(){
    if(tabController.previousIndex == 5){
      tabController.animateTo((0));
    }else{
      tabController.animateTo((tabController.index + 1));
    }
  }
  @override
  void onClose() {
    super.onClose();
    tabController?.dispose();
    constructionAreaController?.dispose();
    exteriorSlidingController?.dispose();
    flooringController?.dispose();
    foundationController?.dispose();
    garagesController?.dispose();
    gradationController?.dispose();
    hVACController?.dispose();
  }
}