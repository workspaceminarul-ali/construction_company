import 'package:construction_company/Controller/MainController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
class Gradation extends StatelessWidget {
  final MainController mainController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
        padding: EdgeInsets.symmetric(horizontal: 20,vertical: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            InkWell(
              onTap: mainController.onSkip,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Skip",
                  style: TextStyle(color: Colors.black),
                ),
              ),
            ),
            InkWell(
              onTap: mainController.onNext,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Next",
                  style: TextStyle(color: Colors.black),
                ),
              ),
            ),
          ],
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("What do u mean by gradation?",style: TextStyle(color: Colors.black),),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 50,horizontal: 50),
            child: TextField(
              controller: mainController.gradationController,
            ),
          ),
        ],
      ),
    );
  }

}