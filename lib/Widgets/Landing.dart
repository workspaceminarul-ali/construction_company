import 'package:construction_company/Controller/MainController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'ConstructionArea.dart';
import 'ExteriorSliding.dart';
import 'Flooring.dart';
import 'Foundation.dart';
import 'Garages.dart';
import 'Gradation.dart';
import 'HVAC.dart';

class Landing extends StatefulWidget {
  Landing({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _LandingState createState() => _LandingState();
}

class _LandingState extends State<Landing>
    with TickerProviderStateMixin<Landing> {
  final MainController mainController = Get.put(MainController());
  @override
  void initState() {
    super.initState();
    mainController.tabController = TabController(length: mainController.lsData.length, vsync: this);
  }

  @override
  void dispose() {
    mainController.tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text(
            "Construction Company",
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white,
          bottom: TabBar(
            controller: mainController.tabController,
            isScrollable: true,
            tabs: mainController.buildTabs(),
          ),
        ),
        body: TabBarView(
            controller: mainController.tabController,
            children: [
              ConstructionArea(),
              Gradation(),
              Foundation(),
              ExteriorSliding(),
              HVAC(),
              Flooring(),
              Garages(),
            ]
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;


}