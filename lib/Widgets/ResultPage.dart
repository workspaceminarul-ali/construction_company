import 'package:construction_company/Controller/MainController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ResultPage extends StatelessWidget {
  final MainController mainController = Get.put(MainController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(
          "Given Data",
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 20,vertical: 20),
        children: [
          Text("How much construction can be done in the total area?",style: TextStyle(color: Colors.black),),
          Text("Given Value: ${mainController.constructionAreaController.text}",style: TextStyle(color: Colors.black),),

          Text("What do u mean by gradation?",style: TextStyle(color: Colors.black),),
          Text("Given Value: ${mainController.gradationController.text}",style: TextStyle(color: Colors.black),),

          Text("What would be the Exterior Siding for House?",style: TextStyle(color: Colors.black),),
          Text("Given Value: ${mainController.foundationController.text}",style: TextStyle(color: Colors.black),),

          Text("How much construction can be done in the total area?",style: TextStyle(color: Colors.black),),
          Text("Given Value: ${mainController.exteriorSlidingController.text}",style: TextStyle(color: Colors.black),),

          Text("How much construction can be done in the total area?",style: TextStyle(color: Colors.black),),
          Text("Given Value: ${mainController.hVACController.text}",style: TextStyle(color: Colors.black),),

          Text("How much construction can be done in the total area?",style: TextStyle(color: Colors.black),),
          Text("Given Value: ${mainController.flooringController.text}",style: TextStyle(color: Colors.black),),

          Text("How Many Garages You Wants to Build",style: TextStyle(color: Colors.black),),
          Text("Given Value: ${mainController.garagesController.text}",style: TextStyle(color: Colors.black),),
        ],
      ),
    );
  }
}
